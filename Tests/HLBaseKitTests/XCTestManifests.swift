import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(HLBaseKitTests.allTests),
        testCase(LinkedListTests.allTests),
        testCase(StackTests.allTests),
        testCase(URLReadTests.allTests)
    ]
}
#endif
