import XCTest
@testable import HLBaseKit

private let timeout = 4.0

final class URLReadTests: XCTestCase {

    static var allTests = [
        ("testSyncData", testSyncData),
        ("testSyncDictionary", testSyncDictionary),
        ("testSyncArray", testSyncArray),
        ("testAsyncData", testAsyncData),
        ("testAsyncDictionary", testAsyncDictionary),
        ("testAsyncArray", testAsyncArray)
    ]

    func testSyncData() {
        let url = TestReader.urlFor(fileName: "Test.json")

        XCTAssertNotNil(try url.data(), "Should not return nil")
    }

    func testSyncDictionary() {
        let url = TestReader.urlFor(fileName: "Test.json")

        let result = try? url.dictionary()
        XCTAssertNotNil(result, "Should not return nil")
        XCTAssertEqual(result?["Test"] as? String, "Hello World", "Value should be there")
    }

    func testSyncArray() {
        let url = TestReader.urlFor(fileName: "Array.json")

        let result = try? url.array()
        XCTAssertNotNil(result, "Should not return nil")
        XCTAssertEqual(result?.first as? String, "Hello World", "Value should be there")
    }

    func testAsyncData() {
        let url = TestReader.urlFor(fileName: "Test.json")

        let expect = expectation(description: "Retrieve data")

        url.data { (result) in
            XCTAssertTrue(!Thread.isMainThread, "Should not be in the main thread")
            switch result {
            case .success(let data):
                if nil != data {
                    expect.fulfill()
                }
            default:
                break
            }
        }

        wait(for: [expect], timeout: timeout)
    }

    func testAsyncDictionary() {
        let url = TestReader.urlFor(fileName: "Test.json")

        let expect = expectation(description: "Retrieve data")

        url.dictionary { (result) in
            XCTAssertTrue(!Thread.isMainThread, "Should not be in the main thread")
            switch result {
            case .success(let dict):
                if nil != dict {
                    XCTAssertEqual(dict?["Test"] as? String, "Hello World", "Value should be a there")
                    expect.fulfill()
                }
            default:
                break
            }
        }

        wait(for: [expect], timeout: timeout)
    }

    func testAsyncArray() {
        let url = TestReader.urlFor(fileName: "Array.json")

        let expect = expectation(description: "Retrieve data")

        url.array { (result) in
            XCTAssertTrue(!Thread.isMainThread, "Should not be in the main thread")
            switch result {
            case .success(let array):
                if nil != array {
                    XCTAssertEqual(array?.first as? String, "Hello World", "Value should be there")
                    expect.fulfill()
                }
            default:
                break
            }
        }

        wait(for: [expect], timeout: timeout)
    }


}
