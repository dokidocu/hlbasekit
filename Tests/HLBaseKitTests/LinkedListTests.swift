import XCTest
@testable import HLBaseKit

final class LinkedListTests: XCTestCase {

    func testInit() {
        let linkedList = LinkedList<Int>()

        XCTAssertNil(linkedList.head, "Head should be nil")
        XCTAssertNil(linkedList.tail, "Tail should be nil")
        XCTAssertTrue(linkedList.isEmpty, "LinkedList should be empty")
    }

    func testPush() {
        var linkedList = LinkedList<Int>()
        let expectedValue = 5
        linkedList.push(expectedValue)

        XCTAssertNotNil(linkedList.head, "Head should be not nil")
        XCTAssertNotNil(linkedList.tail, "Tail should be not nil")
        XCTAssertEqual(linkedList.head?.value, expectedValue, "Value \(expectedValue) is not matching")
        XCTAssertEqual(linkedList.tail?.value, expectedValue, "Value \(expectedValue) is not matching")
    }

    func testAppend() {
        var linkedList = LinkedList<Int>()
        let expectedValue = 5

        linkedList.append(expectedValue)
        XCTAssertNotNil(linkedList.head, "Head should be not nil")
        XCTAssertNotNil(linkedList.tail, "Tail should be not nil")
        XCTAssertEqual(linkedList.head?.value, expectedValue, "Value \(expectedValue) is not matching")
        XCTAssertEqual(linkedList.tail?.value, expectedValue, "Value \(expectedValue) is not matching")

        let expectedSecondValue = 10
        linkedList.append(expectedSecondValue)
        XCTAssertEqual(linkedList.head?.value, expectedValue, "Value \(expectedValue) is not matching")
        XCTAssertEqual(linkedList.tail?.value, expectedSecondValue, "Value \(expectedValue) is not matching")
    }

    func testNodeAt() {
        var linkedList = LinkedList<Int>()

        XCTAssertNil(linkedList.node(at: 0), "Should return nil")
        XCTAssertNil(linkedList.node(at: 100), "Should return nil")
        XCTAssertNil(linkedList.node(at: -100), "Should return nil")

        let expValue1 = 5
        linkedList.append(expValue1)
        let expValue2 = 10
        linkedList.append(expValue2)
        let expValue3 = 100
        linkedList.append(expValue3)

        XCTAssertEqual(linkedList.node(at: 0)?.value, expValue1, "Value \(expValue1) is not matching")
        XCTAssertEqual(linkedList.node(at: 1)?.value, expValue2, "Value \(expValue2) is not matching")
        XCTAssertEqual(linkedList.node(at: 2)?.value, expValue3, "Value \(expValue3) is not matching")
    }

    func testPop() {
        var linkedList = LinkedList<Int>()

        XCTAssertNil(linkedList.pop(), "Should return nil")

        let expValue1 = 5
        linkedList.append(expValue1)
        let expValue2 = 10
        linkedList.append(expValue2)

        XCTAssertEqual(linkedList.pop(), expValue1, "Value \(expValue1) is not matching")
        XCTAssertEqual(linkedList.tail?.value, expValue2, "Value \(expValue2) is not matching")
        XCTAssertEqual(linkedList.head?.value, expValue2, "Value \(expValue2) is not matching")
    }

    func testRemoveLast() {
        var linkedList = LinkedList<Int>()

        XCTAssertNil(linkedList.removeLast(), "Should return nil")

        let expValue1 = 5
        linkedList.append(expValue1)
        let expValue2 = 10
        linkedList.append(expValue2)

        XCTAssertEqual(linkedList.removeLast(), expValue2, "Value \(expValue2) is not matching")
        XCTAssertEqual(linkedList.tail?.value, expValue1, "Value \(expValue1) is not matching")
        XCTAssertEqual(linkedList.head?.value, expValue1, "Value \(expValue1) is not matching")
    }

    func testRemoveAfter() {
        var linkedList = LinkedList<Int>()

        let expValue1 = 5
        linkedList.append(expValue1)
        let expValue2 = 10
        linkedList.append(expValue2)
        let expValue3 = 100
        linkedList.append(expValue3)

        guard let node = linkedList.node(at: 1) else { // expValue == 2
            XCTFail("Should return a node")
            return
        }
        XCTAssertEqual(node.value, expValue2, "Value \(expValue2) is not matching")

        let value = linkedList.remove(after: node)
        XCTAssertEqual(value, expValue3, "Value \(expValue3) is not matching")
    }

    func testHasNoLoop() {
        var linkedList = LinkedList<Int>()
        (1...5).forEach { linkedList.append($0) }
        XCTAssertFalse(linkedList.hasLoop(), "Should not have a loop")
    }

    func testHasLoop() {
        var linkedList = LinkedList<Int>()
        (1...5).forEach { linkedList.append($0) }
        linkedList.tail?.next = linkedList.head
        XCTAssertTrue(linkedList.hasLoop(), "Should not have a loop")
    }

    static var allTests = [
        ("testInit", testInit),
        ("testPush", testPush),
        ("testAppend", testAppend),
        ("testNodeAt", testNodeAt),
        ("testPop", testPop),
        ("testRemoveLast", testRemoveLast),
        ("testHasNoLoop", testHasNoLoop),
        ("testHasLoop", testHasLoop)
    ]

}
