//
//  File.swift
//  
//
//  Created by Henri LA on 24.01.20.
//

import Foundation

class TestReader {
  
  static func urlFor(fileName: String, at path: String = #file) -> URL {
    return URL(fileURLWithPath: path, isDirectory: false)
      .deletingLastPathComponent()
      .appendingPathComponent(fileName, isDirectory: false)
  }
  
}
