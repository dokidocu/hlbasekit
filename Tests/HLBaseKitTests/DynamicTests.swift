import XCTest
@testable import HLBaseKit

final class DynamicTests: XCTestCase {

    func testInit() {
        let expValue = 100
        let dynamicValue = Dynamic<Int>(expValue)
        XCTAssertEqual(expValue, dynamicValue.value, "Value should be the same")

        XCTAssertNil(Dynamic<Int>(nil).value, "Value should be the same")
    }

    func testBindAndFire() {
        let exp = expectation(description: "Fired")
        let expValue = 100
        let dynamicValue = Dynamic<Int>(expValue)
        var receiver: Int? = 0

        XCTAssertEqual(receiver, 0, "Value should be the same")
        dynamicValue.bindAndFire { (value) in
            receiver = value
            XCTAssertEqual(receiver, expValue, "Value should be the same")
            exp.fulfill()
        }

        XCTAssertEqual(receiver, expValue, "Value should be the same")
        
        wait(for: [exp], timeout: 2.0)
    }

    func testBind() {
        let exp = expectation(description: "Fired")
        let expValue = 100
        let dynamicValue = Dynamic<Int>(nil)
        var receiver: Int? = 0

        XCTAssertNil(dynamicValue.value, "Value should be nil")

        dynamicValue.bind { (value) in
            receiver = value
            exp.fulfill()
        }

        XCTAssertEqual(receiver, 0, "Value should be the same")
        dynamicValue.value = expValue
        XCTAssertEqual(receiver, expValue, "Value should be the same")

        wait(for: [exp], timeout: 2.0)
    }

    static var allTests = [
        ("testInit", testInit),
        ("testBindAndFire", testBindAndFire),
        ("testBind", testBind)
    ]

}
