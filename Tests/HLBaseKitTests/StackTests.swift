import XCTest
@testable import HLBaseKit

final class StackTests: XCTestCase {

    func testInit() {
        var stack = Stack<Int>()

        XCTAssertNil(stack.pop(), "Stack should be empty")
        XCTAssertNil(stack.peek(), "Stack should be empty")
        XCTAssertTrue(stack.isEmpty, "Stack should be empty")
    }

    func testPeek() {
        var stack = Stack<Int>()
        XCTAssertNil(stack.peek(), "Should return nil")

        stack.push(1)
        let expValue = 100
        stack.push(expValue)

        XCTAssertEqual(stack.peek(), expValue, "Value should be the same")
        XCTAssertEqual(stack.count, 2, "Count should be the same")
    }

    func testPush() {
        var stack = Stack<Int>()
        let expValue = 100
        stack.push(expValue)

        XCTAssertEqual(stack.peek(), expValue, "Value should be the same")
        XCTAssertEqual(stack.count, 1, "Count should be the same")
    }

    func testPop() {
        var stack = Stack<Int>()
        let nilValue = stack.pop()
        XCTAssertNil(nilValue, "Should return nil")

        let expValue = 100
        stack.push(expValue)

        let popValue = stack.pop()
        XCTAssertEqual(popValue, expValue, "Value should be the same")
        XCTAssertEqual(stack.count, 0, "Count should be the same")
    }

    static var allTests = [
        ("testInit", testInit),
        ("testPeek", testPeek),
        ("testPush", testPush),
        ("testPop", testPop)
    ]

}
