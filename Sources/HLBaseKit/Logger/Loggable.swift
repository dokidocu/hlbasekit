import Foundation

public protocol Loggable {
  
  func logDebug(message: String, category: String, file: String, function: String, line: Int)
  func logVerbose(message: String, category: String, file: String, function: String, line: Int)
  func logError(message: String, category: String, file: String, function: String, line: Int)
  func logWarning(message: String, category: String, file: String, function: String, line: Int)
  
}

public extension Loggable {
  
  func logDebug(message: String, category: String, file: String = #file, function: String = #function, line: Int = #line) {
    logDebug(message: message, category: category, file: file, function: function, line: line)
  }
  
  func logVerbose(message: String, category: String, file: String = #file, function: String = #function, line: Int = #line) {
    logVerbose(message: message, category: category, file: file, function: function, line: line)
  }
  
  func logError(message: String, category: String, file: String = #file, function: String = #function, line: Int = #line) {
    logError(message: message, category: category, file: file, function: function, line: line)
  }
  
  func logWarning(message: String, category: String, file: String = #file, function: String = #function, line: Int = #line) {
    logWarning(message: message, category: category, file: file, function: function, line: line)
  }
  
}
