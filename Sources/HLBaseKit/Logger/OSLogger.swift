//
//  HLBaseLogger.swift
//  
//
//  Created by Henri LA on 23.01.20.
//

import Foundation
import os

public struct OSLogger: Loggable {

    private let showFileAndFunction: Bool

    public init?(showFileAndFunction: Bool = false) {
        #if !DEBUG
        return nil
        #else
        self.showFileAndFunction = showFileAndFunction
        #endif
    }

    // MARK: HLBaseLoggable

    /**
     Log to the console a debug message with a specific category

     - Parameters:
        - message: a String message
        - category: a String category
     */
    public func logDebug(message: String, category: String, file: String = #file, function: String = #function, line: Int = #line) {
        logMessage(message, category: category, for: .debug, file: file, function: function)
    }

    /**
     Log to the console a verbose message with a specific category

     - Parameters:
        - message: a String message
        - category: a String category
     */
    public func logVerbose(message: String, category: String, file: String = #file, function: String = #function, line: Int = #line) {
        logMessage(message, category: category, for: .info, file: file, function: function)
    }

    /**
     Log to the console an error message with a specific category

     - Parameters:
        - message: a String message
        - category: a String category
     */
    public func logError(message: String, category: String, file: String = #file, function: String = #function, line: Int = #line) {
        logMessage(message, category: category, for: .error, file: file, function: function)
    }

    /**
     Log to the console a warning message with a specific category

     - Parameters:
        - message: a String message
        - category: a String category
     */
    public func logWarning(message: String, category: String, file: String = #file, function: String = #function, line: Int = #line) {
        logMessage(message, category: category, for: .fault, file: file, function: function)
    }

    // MARK: Static

    private static var logger = OSLogger()

    /**
     Log to the console a debug message with a specific category

     - Parameters:
        - message: a String message
        - category: a String category
     */
    public static func logDebug(message: String, category: String, file: String = #file, function: String = #function, line: Int = #line) {
        logger?.logMessage(message, category: category, for: .debug, file: file, function: function)
    }

    /**
     Log to the console a verbose message with a specific category

     - Parameters:
        - message: a String message
        - category: a String category
     */
    public static func logVerbose(message: String, category: String, file: String = #file, function: String = #function, line: Int = #line) {
        logger?.logMessage(message, category: category, for: .info, file: file, function: function)
    }

    /**
     Log to the console an error message with a specific category

     - Parameters:
        - message: a String message
        - category: a String category
     */
    public static func logError(message: String, category: String, file: String = #file, function: String = #function, line: Int = #line) {
        logger?.logMessage(message, category: category, for: .error, file: file, function: function)
    }

    /**
     Log to the console a warning message with a specific category

     - Parameters:
        - message: a String message
        - category: a String category
     */
    public static func logWarning(message: String, category: String, file: String = #file, function: String = #function, line: Int = #line) {
        logger?.logMessage(message, category: category, for: .fault, file: file, function: function)
    }

    // MARK: Private functions

    private func logMessage(_ message: String, category: String, for type: OSLogType, file: String, function: String) {
        if showFileAndFunction {
            os_log(type,
                   log: OSLog(subsystem: Bundle.mainBundleIdentifier, category: category),
                   "#%@<%@, %@>: %@", threadInfo(), prefixLog(file: file), function, message)
        } else {
            os_log(type,
                   log: OSLog(subsystem: Bundle.mainBundleIdentifier, category: category),
                   "#%@: %@", threadInfo(), message)
        }
    }

    private func threadInfo() -> String {
        let result: String

        if Thread.isMainThread {
            result = "Main"
        } else {
            result = "Background"
        }

        return result
    }

    private func prefixLog(file: String) -> String {
        return "[\(threadInfo()), \(URL(fileURLWithPath: file).pathName)]"
    }

}

private extension Bundle {

    static var mainBundleIdentifier: String {
        return Bundle.main.bundleIdentifier ?? ""
    }

}

private extension URL {
    var pathName: String {
        guard let value = self.lastPathComponent.split(separator: ".").first else { return lastPathComponent }
        return String(value)
    }
}
