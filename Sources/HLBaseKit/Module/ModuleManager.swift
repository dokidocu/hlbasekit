//
//  ModuleManager.swift
//  HLUIKit
//
//  Created by Henri LA on 10.05.20.
//  Copyright © 2020 Henri La. All rights reserved.
//

final public class ModuleManager {

    // MARK: Parameters

    private var modules: [String: Modulable] = [:]

    // MARK: Singleton / Init

    private init() {}

    public static var shared = ModuleManager()

    // MARK: Registration

    public func registerModule(_ module: Modulable) {
        modules[String(describing: type(of: module))] = module
    }

    public func clearRegisteredModules() {
        modules.removeAll()
    }

    // MARK: Execution

    public func execute(_ moduleOrder: ModuleOrder) -> Any? {
        let arrayModules = modules.compactMap{ $1 }
        let excutableModule = arrayModules.first(where: { $0.canExecute(moduleOrder) })

        return excutableModule?.execute(moduleOrder)
    }

    public func execute(_ moduleOrder: ModuleOrder,
                        completion: GenericCompletionHandler<Any?>?)
    {
        let arrayModules = modules.compactMap{ $1 }
        let excutableModule = arrayModules.first(where: { $0.canExecute(moduleOrder) })

        excutableModule?.execute(moduleOrder, completion: completion)
    }

    public func module(_ classModule: Any) -> Modulable? {
        return modules[String(describing: classModule)]
    }

}
