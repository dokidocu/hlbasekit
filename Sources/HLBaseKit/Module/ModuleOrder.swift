//
//  ModuleAction.swift
//  HLUIKit
//
//  Created by Henri LA on 13.05.20.
//  Copyright © 2020 Henri La. All rights reserved.
//

import Foundation
import UIKit

public enum ModuleOrder {

    case message(identifier: String)
    case string(identifier: String, obj: String)
    case double(identifier: String, obj: Double)
    case float(identifier: String, obj: Float)
    case cgfloat(identifier: String, obj: CGFloat)
    case integer(identifier: String, obj: Int)
    case data(identifier: String, obj: Data)
    case any(identifier: String, obj: Any)

}
