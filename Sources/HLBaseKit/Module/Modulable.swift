//
//  Modulable.swift
//  HLUIKit
//
//  Created by Henri LA on 10.05.20.
//  Copyright © 2020 Henri La. All rights reserved.
//

import UIKit

public protocol Modulable {

    func canExecute(_ moduleOrder: ModuleOrder) -> Bool

    func execute(_ moduleOrder: ModuleOrder) -> Any?
    func execute(_ moduleOrder: ModuleOrder, completion: GenericCompletionHandler<Any?>?)

    var moduleCategory: ModuleCategory { get }
}

public enum ModuleCategory {

    case ui
    case data
    case debug
    case validator

    var isVisible: Bool {
        switch self {
        case .ui:
            return true
        default:
            return false
        }
    }

}
