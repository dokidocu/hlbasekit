//
//  CacheConfiguration.swift
//  
//
//  Created by Henri LA on 21.04.20.
//

import Foundation

public struct CacheConfiguration {
  let policy: URLRequest.CachePolicy
  let expirationDate: Date
  
  static public var noConfig: CacheConfiguration {
    return CacheConfiguration(policy: .reloadIgnoringLocalCacheData, expirationDate: Date.distantPast)
  }
}
