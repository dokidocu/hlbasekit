//
//  HLNResponse.swift
//  
//
//  Created by Henri LA on 21.04.20.
//

import Foundation

public struct HLNResponse {
  public let response: URLResponse?
  public let data: Data?
  public let error: Error?
}
