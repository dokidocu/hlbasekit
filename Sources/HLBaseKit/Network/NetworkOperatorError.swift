//
//  NetworkOperatorError.swift
//  
//
//  Created by Henri LA on 09.02.20.
//

import Foundation

enum NetworkOperatorError: Error {
  case noCacheData
  case fatalError
  
  var localizedDescription: String {
    let result: String
    switch self {
    case .noCacheData:
      result = NSLocalizedString("No cache data available",
                                 comment: "HLNNetworkError - No cache data")
    case .fatalError:
      result = NSLocalizedString("System error - no data and response",
                                 comment: "HLNNetworkError - no data, no response and no error!")
    }
    
    return result
  }
}
