//
//  NetworkOperator.swift
//  
//
//  Created by Henri LA on 08.02.20.
//

import Foundation
import TrustKit

public extension Notification.Name {
    // Connections
    static let HLBaseNetworkConnectionLost = Notification.Name(rawValue: "HLBaseNetworkConnectionLost")
    static let HLBaseNetworkConnectionAvailable = Notification.Name(rawValue: "HLBaseNetworkConnectionAvailable")

    // Pinning & Session
    static let HLBaseNetworkInvalidPinning = Notification.Name(rawValue: "HLBaseNetworkInvalidPinning")
    static let HLBaseNetworkSessionInvalidated = Notification.Name("HLBaseNetworkSessionInvalidated")

    static let HLBaseNetworkSessionDidFinishEvents = Notification.Name(rawValue: "HLBaseNetworkSessionDidFinishEvents")
}

public class NetworkOperator: NSObject {

    private let reachability: Reachability? = try? Reachability()
    private let notificationCenter = NotificationCenter.default

    // MARK: Public Constants & variables

    let configuration: URLSessionConfiguration
    let sessionQueue: OperationQueue
    let cache: URLCache?
    let operatorQueue: OperationQueue
    let pinningValidator: TSKPinningValidator?

    private lazy var session: URLSession = {
        return URLSession(configuration: configuration, delegate: self, delegateQueue: sessionQueue)
    }()

    /// pendingRequests should only be accessible via OperationQueue ! -> operatorQueue
    private var pendingRequests = [URLRequest : URLSessionDataTask]()

    public init(configuration: URLSessionConfiguration,
                sessionQueue: OperationQueue,
                cache: URLCache? = nil,
                operatorQueue: OperationQueue = .semaphore(name: "Response queue handler"),
                pinningValidator: TSKPinningValidator? = nil)
    {
        self.configuration = configuration
        self.sessionQueue = sessionQueue
        self.cache = cache
        self.operatorQueue = operatorQueue
        self.pinningValidator = pinningValidator

        super.init()

        // Configure reachbility
        reachability?.allowsCellularConnection = configuration.allowsCellularAccess

        reachability?.whenUnreachable = { [weak self] reachability in
            self?.notificationCenter.post(name: .HLBaseNetworkConnectionLost, object: reachability)
        }

        reachability?.whenReachable = { [weak self] reachability in
            self?.notificationCenter.post(name: .HLBaseNetworkConnectionAvailable, object: reachability)
        }
    }

    public func sendRequest(_ urlRequest: URLRequest,
                            completion: @escaping (HLNResponse) -> () )
    {
        let hlnResponse: HLNResponse
        let cachePolicy = urlRequest.cachePolicy

        if cachePolicy == .returnCacheDataElseLoad || cachePolicy == .returnCacheDataDontLoad,
           let cachedResponse = cache?.cachedResponse(for: urlRequest)
        {
            hlnResponse = HLNResponse(response: cachedResponse.response,
                                      data: cachedResponse.data,
                                      error: nil)
            completion(hlnResponse)
            return
        }

        // No cache data available and according to the policy, the request fails !
        if cachePolicy == .returnCacheDataDontLoad {
            hlnResponse = HLNResponse(response: nil,
                                      data: nil,
                                      error: NetworkOperatorError.noCacheData)
            completion(hlnResponse)
            return
        }

        // Get the cache failed -> Store the cache when possible
        let willCacheData = cachePolicy == .returnCacheDataElseLoad

        // No cache data or .getCache is ignored
        let dataTask = session.dataTask(with: urlRequest) { (data, response, error) in
            let hlnResponse: HLNResponse

            // Check if error
            if let error = error {
                hlnResponse = HLNResponse(response: response, data: data, error: error)
            }
            else if let response = response, let data = data { // Response and data are here

                // Cache data if cachePolicy is fitting the requirement
                if willCacheData {
                    let cachedURLResponse = CachedURLResponse(response: response, data: data)
                    self.cache?.storeCachedResponse(cachedURLResponse, for: urlRequest)
                }

                hlnResponse = HLNResponse(response: response, data: data, error: error)
            } else {
                hlnResponse = HLNResponse(response: response,
                                          data: data,
                                          error: NetworkOperatorError.fatalError)
            }

            completion(hlnResponse)
        }

        // Monitor any sessionDataTasks created
        addSessionDataTask(dataTask, forRequest: urlRequest)

        dataTask.resume()
    }

    public func numberOfPendingRequests(_ completion: @escaping (Int) -> ()) {
        operatorQueue.addOperation { [weak self] in
            let total = self?.pendingRequests.count ?? 0
            completion(total)
        }

    }

    public func cancelAllPendingRequests(){
        operatorQueue.addOperation { [weak self] in
            self?.pendingRequests.forEach{ $0.value.cancel() }
            self?.pendingRequests.removeAll()
        }
    }

    // MARK: Private functions

    private func addSessionDataTask(_ sessionDataTask: URLSessionDataTask,
                                    forRequest request: URLRequest)
    {
        operatorQueue.addOperation { [weak self] in
            self?.pendingRequests[request] = sessionDataTask
        }
    }

    private func removeSessionDataTask(_ sessionDataTask: URLSessionDataTask,
                                       forRequest request: URLRequest)
    {
        operatorQueue.addOperation { [weak self] in
            self?.pendingRequests.removeValue(forKey: request)
        }
    }

}

extension NetworkOperator: URLSessionDelegate {

    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {

        guard let pinningValidator = pinningValidator else {
            completionHandler(.performDefaultHandling, nil)
            return
        }

        #if DEBUG

        if let secTrust = challenge.protectionSpace.serverTrust,
           pinningValidator.evaluateTrust(secTrust, forHostname: challenge.protectionSpace.host) == .shouldAllowConnection
        {
            completionHandler(.performDefaultHandling, nil)
        } else {
            notificationCenter.post(name: .HLBaseNetworkInvalidPinning,
                                    object: challenge.protectionSpace.host)
            completionHandler(.cancelAuthenticationChallenge, nil)
        }

        #else
        if !pinningValidator.handle(challenge, completionHandler: completionHandler) {
            // Missing pinning ? -> Inform what url was not pinned
            notificationCenter.post(name: .HLBaseNetworkInvalidPinning,
                                    object: challenge.protectionSpace.host)

            completionHandler(.cancelAuthenticationChallenge, nil)
        }
        #endif

    }

    public func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        notificationCenter.post(name: .HLBaseNetworkSessionInvalidated, object: error)
    }

    public func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        DispatchQueue.main.async { [weak self] in
            self?.notificationCenter.post(name: .HLBaseNetworkSessionDidFinishEvents, object: session)
        }
    }

}


public extension NetworkOperator {

    static func securedNetworkOperator(validator: TSKPinningValidator) -> NetworkOperator {
        let sessionQueue = OperationQueue()
        sessionQueue.qualityOfService = .userInitiated

        return NetworkOperator(configuration: .ephemeral,
                               sessionQueue: sessionQueue,
                               pinningValidator: validator)
    }

}
