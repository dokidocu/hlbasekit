//
//  GenericClosures.swift
//  
//
//  Created by Henri LA on 27.04.20.
//

import Foundation

public typealias GenericCompletionHandler<T> = (T) -> ()
public typealias CompletionHandler = () -> ()

public typealias BoolCompletionHandler = GenericCompletionHandler<Bool>
public typealias StringCompletionHandler = GenericCompletionHandler<String?>
public typealias IntCompletionHandler = GenericCompletionHandler<Int>
public typealias DoubleCompletionHandler = GenericCompletionHandler<Double>
public typealias DataCompletionHandler = GenericCompletionHandler<Data>
public typealias ErrorCompletionHandler = GenericCompletionHandler<Error?>

// MARK: - Throwable Closures
public typealias ThrowableCompletionHandler = () throws -> Void

