//
//  File.swift
//  
//
//  Created by Henri LA on 24.09.20.
//

import Foundation

open class MultiSectionDataSource<T>: GenericDataSourceable {

    public var source: [Int: [T]] = [Int: [T]]()

    public init() {}

    public func numberOfSections() -> Int {
        return source.count
    }

    public func numberOfItems(in section: Int) -> Int {
        return source[section]?.count ?? 0
    }

    public func item(at indexPath: IndexPath) -> T? {
        return source[indexPath.section]?[indexPath.row]
    }

}

open class OneSectionDataSource<T>: GenericDataSourceable {

    public var source: [T] = [T]()

    public init() {}

    public func numberOfSections() -> Int {
        return 1
    }

    public func numberOfItems(in section: Int) -> Int {
        return source.count
    }

    public func item(at indexPath: IndexPath) -> T? {
        return source[indexPath.row]
    }

}
