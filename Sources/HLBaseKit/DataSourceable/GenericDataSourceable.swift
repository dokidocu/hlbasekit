//
//  GenericDataSourceable.swift
//  
//
//  Created by Henri LA on 23.01.20.
//

import Foundation

public protocol GenericDataSourceable {
  associatedtype T
  
  func numberOfSections() -> Int
  func numberOfItems(in section: Int) -> Int
  func item(at indexPath: IndexPath) -> T?
}

public protocol MultiGenericDataSourceable {
  associatedtype K: Hashable
  associatedtype V
  
  // MARK: Init
  
  init(source: [K: [V]])
  
  // MARK: Data
  
  func numberOfSections() -> Int
  func numberOfItems(in section: Int) -> Int
  func item(at indexPath: IndexPath) -> V?
  
}

public protocol HeaderFooterSourceable {
  
  func titleForHeaderInSection(_ section: Int) -> String?
  func titleForFooterInSection(_ section: Int) -> String?
  
}

public protocol IndexSourceable {
  
  func sectionIndexTitles() -> [String]?
  func sectionForSectionIndexTitle(_ indexTitle: String, at index: Int) -> Int
  
}
