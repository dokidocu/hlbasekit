//
//  Stack.swift
//  
//
//  Created by Henri LA on 16.02.21.
//

import Foundation

public struct Stack<Element> {

    private var storage: [Element] = []

    // MARK: Init

    public init() {}

    public init(_ elements: [Element]) {
        storage = elements
    }

    // MARK: Public functions / properties

    /**
     Returns true if the stack is empty. Otherwise, it returns false

     - Returns: a Bool
     */
    public var isEmpty: Bool {
        return nil == peek()
    }

    /**
     Returns the total Elements in the stack

     - Returns: an Int
     */
    public var count: Int {
        return storage.count
    }

    /**
     Returns an Element from the top of the Stack without changing the stack``

     - Returns: an Element or nil
     */
    public func peek() -> Element? {
        return storage.last
    }

    /**
     Push an element at the end of the stack

     - Parameters:
        - element: as Element
     */
    public mutating func push(_ element: Element) {
        storage.append(element)
    }

    /**
     Returns the last element of the stack

     - Returns: Element or nil
     */
    @discardableResult
    public mutating func pop() -> Element? {
        return storage.popLast()
    }

}

// MARK: ExpressibleByArrayLiteral

extension Stack: ExpressibleByArrayLiteral {

    public init(arrayLiteral elements: Element...) {
        storage = elements
    }

}

// MARK: CustomStringConvertible

extension Stack: CustomStringConvertible {

    public var description: String {
        let topDiviser = "----- top -----\n"
        let bottomDiviser = "\n---------------"

        let stackElements = storage
            .map{ "\($0)" }
            .reversed()
            .joined(separator: "\n")

        return topDiviser + stackElements + bottomDiviser
    }

}
