import Foundation

final public class Dynamic<Value> {

    // MARK: Properties

    public typealias Listener = (Value?) -> Void

    private var listener: Listener?
    public var value: Value? {
        didSet {
            listener?(value)
        }
    }

    // MARK: Init

    public init(_ value: Value?) {
        self.value = value
    }

    deinit {
        listener = nil
        value = nil
    }

    // MARK: Public functions

    /**
     Bind a Listener

     - Parameters:
        - listener: a Listener = (Value?) -> Void
     */
    public func bind(_ listener: @escaping Listener) {
        self.listener = listener
    }

    /**
     Bind a Listener and get the latest value right away

     - Parameters:
        - listener: a Listener = (Value?) -> Void
     */
    public func bindAndFire(_ listener: @escaping Listener) {
        bind(listener)
        self.listener?(value)
    }

}
