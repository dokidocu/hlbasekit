//
//  LinkedList.swift
//  
//
//  Created by Henri LA on 16.02.21.
//

import Foundation

public struct LinkedList<Value> {

    // MARK: Read properties

    private(set) public var head: Node<Value>?
    private(set) public var tail: Node<Value>?

    public var isEmpty: Bool {
        return head == nil
    }

    // MARK: Init

    public init() {}

    // MARK: Mutable functions

    /**
     Push a new value / node at the beginning of the list

     - Parameters:
        - value: a value
     */
    public mutating func push(_ value: Value) {
        copyNodes()

        head = Node(value, next: head)
        if nil == tail {
            tail = head
        }
    }

    /**
     Append at the end of list a new value / Node

     - Parameters:
        - value: a  Value
     */
    public mutating func append(_ value: Value) {
        copyNodes()

        guard !isEmpty else {
            push(value)
            return
        }

        tail?.next = Node(value)

        tail = tail?.next
    }

    /**
     Returns a `node` or nil at a specific index Int

     - Parameters:
        - index: Int
     - Returns: a Node or nil
     */
    public func node(at index: Int) -> Node<Value>? {
        var currentNode = head
        var currentIndex = 0

        while nil != currentNode && currentIndex < index {
            currentNode = currentNode?.next
            currentIndex += 1
        }

        return currentNode
    }

    /**
     Returns a new `node` after inserting the new node at a particular place in the list

     - Parameters:
        - value: a Value
        - node : a  Node
     - Returns: a new Node
     */
    @discardableResult
    public mutating func insert(_ value: Value,
                                after node: Node<Value>) -> Node<Value>
    {
        copyNodes()

        guard tail !== node else {
            append(value)
            return tail!
        }

        node.next = Node(value, next: node.next)
        return node.next!
    }

    /**
     Returns a value at the front of the list
     - Returns: Value?
     */
    @discardableResult
    public mutating func pop() -> Value? {
        defer {
            head = head?.next
            if isEmpty {
                tail = nil
            }
        }
        copyNodes()

        return head?.value
    }

    /**
     Remove the last node of the list by returning its value
     - Returns: a Value or nil
     */
    @discardableResult
    public mutating func removeLast() -> Value? {
        copyNodes()

        guard let head = head else {
            return nil
        }

        guard nil != head.next else {
            return pop()
        }

        var previousNode = head
        var currentNode = head

        while let next = currentNode.next {
            previousNode = currentNode
            currentNode = next
        }

        previousNode.next = nil
        tail = previousNode

        return currentNode.value
    }

    /**
     Removes a node from the list after a specified node and returns a Value or nil

     - Parameters:
     - a node: Node
     - Returns: a Value or nil
     */
    @discardableResult
    public mutating func remove(after node: Node<Value>) -> Value? {
        defer {
            if node.next === tail {
                tail = node
            }
            node.next = node.next?.next
        }
        copyNodes()

        return node.next?.value
    }

    // MARK: Copy on write

    private mutating func copyNodes() {
        guard !isKnownUniquelyReferenced(&head) else {
            return
        }

        guard var oldNode = head else {
            return
        }

        head = Node(oldNode.value)
        var newNode = head

        while let nextOldNode = oldNode.next {
            newNode?.next = Node(nextOldNode.value)
            newNode = newNode?.next

            oldNode = nextOldNode
        }

        tail = newNode
    }

    // MARK: Loop detection

    /**
     Check if the last node is connected to the first node and returns true if it is the case. Otherwise, it returns false
     - Returns:a Bool
     */
    public func hasLoop() -> Bool {
        guard !isEmpty else {
            return false
        }

        return tail?.next === head
    }
}

// MARK: Collection

extension LinkedList: Collection {

    public struct Index: Comparable {

        public var node: Node<Value>?

        static public func ==(lhs: Index, rhs: Index) -> Bool {
            switch (lhs.node, rhs.node) {
            case let (left?, right?):
                return left.next === right.next
            case (nil, nil):
                return true
            default:
                return false
            }
        }

        static public func <(lhs: Index, rhs: Index) -> Bool {
            guard lhs != rhs else {
                return false
            }

            let nodes = sequence(first: lhs.node) { $0?.next }
            return nodes.contains { $0 === rhs.node }
        }
    }

    public var startIndex: Index {
        return Index(node: head)
    }

    public var endIndex: Index {
        return Index(node: tail?.next)
    }

    public func index(after i: Index) -> Index {
        return Index(node: i.node?.next)
    }

    public subscript(position: Index) -> Value? {
        return position.node?.value
    }
}

// MARK: - Node

final public class Node<Value> {

    private(set) public var value: Value
    public var next: Node?

    public init(_ value: Value, next: Node? = nil) {
        self.value = value
        self.next = next
    }

}

// MARK: - CustomStringConvertible

extension Node: CustomStringConvertible {

    public var description: String {
        guard let next = next else {
            return "\(value)"
        }

        return "\(value) -> " + String(describing: next)
    }

}

extension LinkedList: CustomStringConvertible {

    public var description: String {
        guard let head = head else {
            return "LinkedList is empty"
        }

        return String(describing: head)
    }

}
