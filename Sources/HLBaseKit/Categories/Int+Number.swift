//
//  Int+Number.swift
//  
//
//  Created by Henri LA on 10.01.21.
//

import Foundation

public extension Int {

    var isOdd: Bool {
        return !isEven
    }

    var isEven: Bool {
        return self % 2 == 0
    }

}
