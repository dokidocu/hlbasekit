//
//  OperationQueue+Semaphore.swift
//  
//
//  Created by Henri LA on 29.02.20.
//

import Foundation

public extension OperationQueue {
  
  static func semaphore(name: String, qualityOfService: QualityOfService = .userInitiated) -> OperationQueue {
    let operationQueue = OperationQueue()
    operationQueue.name = name
    operationQueue.maxConcurrentOperationCount = 1
    operationQueue.qualityOfService = qualityOfService
    return operationQueue
  }
  
}
