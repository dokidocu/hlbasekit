//
//  Locale+Language.swift
//  HLKit
//
//  Created by Henri LA on 28.09.19.
//

import Foundation

public extension Locale {
  
  /// Returns the system language of the device (en, fr, de, ...)
  static var systemLanguageCode : String {
    guard let lang = Locale.preferredLanguages.first else { return "" }
    
    if lang.count > 2 {
      let index : String.Index = lang.index(lang.startIndex, offsetBy: 2)
      return String(lang[..<index])
    }
    
    return lang
  }
  
}
