import Foundation

// MARK: - Date/NSDate

public func == (lhs: Date, rhs: NSDate) -> Bool {
  let nsDateToDate: Date = rhs as Date
  return lhs.compare(nsDateToDate) == .orderedSame
}

public func < (lhs: Date, rhs: NSDate) -> Bool {
  let nsDateToDate: Date = rhs as Date
  return lhs.compare(nsDateToDate) == .orderedAscending
}

public func <= (lhs: Date, rhs: NSDate) -> Bool {
  let nsDateToDate: Date = rhs as Date
  return lhs.compare(nsDateToDate) != .orderedDescending
}

public func > (lhs: Date, rhs: NSDate) -> Bool {
  let nsDateToDate: Date = rhs as Date
  return lhs.compare(nsDateToDate) == .orderedDescending
}

public func >= (lhs: Date, rhs: NSDate) -> Bool {
  let nsDateToDate: Date = rhs as Date
  return lhs.compare(nsDateToDate) != .orderedAscending
}
