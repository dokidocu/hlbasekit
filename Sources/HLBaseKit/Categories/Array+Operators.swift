import Foundation

public func += <T>(left: inout [T], right: T) {
  left += [right]
}
