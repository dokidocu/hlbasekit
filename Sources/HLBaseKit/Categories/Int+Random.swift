//
//  Int+Random.swift
//  HLKit
//
//  Created by Henri LA on 28.09.19.
//

import Foundation

public extension Int {
  
  /// Returns a random number between 0 and a value not included
  /// => [0, value[
  static func randomUntil(_ value : Int) -> Int {
    return Int( arc4random() % UInt32(value) )
  }
  
}
