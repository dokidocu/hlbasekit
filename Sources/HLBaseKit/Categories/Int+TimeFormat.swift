import Foundation

public extension Int {

    var minutes: TimeInterval {
        return TimeInterval(self * 60)
    }

    var hours: TimeInterval {
        return TimeInterval(self * 60 * 60)
    }

    var days: TimeInterval {
        return TimeInterval(self * 24 * 3600)
    }

    var weeks: TimeInterval {
        return TimeInterval(Double(self) * 7.days)
    }

}
