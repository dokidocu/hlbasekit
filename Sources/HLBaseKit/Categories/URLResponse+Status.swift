//
//  URLResponse+Status.swift
//  
//
//  Created by Henri LA on 09.02.20.
//

import Foundation

public extension URLResponse {
  
  /// The response’s HTTP status code.
  var code: Int {
    guard let httpResponse = self as? HTTPURLResponse else {
      return -1
    }
    
    return httpResponse.statusCode
  }
  
}
