public extension Int {
  /// Returns value as Byte in UInt
  var B: UInt {
    return self >= 0 ? UInt(self) : 0
  }

  /// Returns value as KiloByte in UInt
  var KB: UInt {
    return B * 1024
  }

  /// Returns value as MegaByte in UInt
  var MB: UInt {
    return KB * 1024
  }

  /// Returns value as GigaByte in UInt
  var GB: UInt {
    return MB * 1024
  }
}
