//
//  FileManager+TempFile.swift
//  
//
//  Created by Henri LA on 16.01.21.
//

import Foundation

public enum FileManagerError: LocalizedError {
    case fileDoesNotExist
    case noAccessSecurityScopedResource

    public var errorDescription: String? {
        switch self {
        case .fileDoesNotExist:
            return NSLocalizedString(key: "The file does not exist",
                                     comment: "FileManagerError, file cannot be opened")
        case .noAccessSecurityScopedResource:
            return NSLocalizedString(key: "The file cannot be securily opened",
                                     comment: "FileManagerError, not possible to open secured file")
        }
    }
}

public extension FileManager {

    class func saveTemporaryFile(from sourceURL: URL, with name: String? = nil) throws -> URL {
        return try self.saveTemporaryFile(from: sourceURL, with: name)
    }

    func saveTemporaryFile(from sourceURL: URL, with name: String? = nil) throws -> URL {
        guard sourceURL.startAccessingSecurityScopedResource() else {
            throw FileManagerError.noAccessSecurityScopedResource
        }

        defer { sourceURL.stopAccessingSecurityScopedResource() }

        guard fileExists(atPath: sourceURL.path) else {
            throw FileManagerError.fileDoesNotExist
        }

        let fileName: String
        if let name = name {
            fileName = name
        } else {
            fileName = sourceURL.lastPathComponent
        }

        let destinationURL = temporaryDirectory.appendingPathComponent(fileName)
        if fileExists(atPath: destinationURL.path) {
            try removeItem(at: destinationURL)
        }
        try copyItem(at: sourceURL, to: destinationURL)

        return destinationURL
    }

}
