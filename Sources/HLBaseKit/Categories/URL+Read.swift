import Foundation

public extension URL {
    /**
     Returns synchronously a data  based on URL

     - Returns: Data or nil
     */
    func data() throws -> Data? {
        return try Data(contentsOf: self,
                        options: .mappedIfSafe)
    }

    /**
     Returns synchronously a dictionary of [String: Any]? based on URL

     - Returns: [String: Any] or nil
     */
    func dictionary() throws -> [String: Any]? {
        guard let data = try data() else {
            return nil
        }

        return try JSONSerialization.jsonObject(with: data,
                                                options: []) as? [String: Any]
    }

    /**
     Returns synchronously an array of [Any]? based on URL

     - Returns: [String: Any] or nil
     */
    func array() throws -> [Any]? {
        guard let data = try data() else {
            return nil
        }

        return try JSONSerialization.jsonObject(with: data,
                                                options: []) as? [Any]
    }

}

// MARK: - Async

public extension URL {
    /**
     Retrieve asynchronously data at URL

     - Parameters:
     - completion: a Result<Data?, Error> -> Void
     */
    func data(completion: @escaping (Result<Data?, Error>) -> Void) {
        DispatchQueue.global(qos: .background).async {
            do {
                let result = try data()
                completion(.success(result))
            } catch let error {
                completion(.failure(error))
            }
        }
    }

    /**
     Retrieve asynchronously a dictionary at URL

     - Parameters:
     - completion: a Result<[String: Any]?, Error> -> Void
     */
    func dictionary(completion: @escaping (Result<[String: Any]?, Error>) -> Void) {
        DispatchQueue.global(qos: .background).async {
            do {
                let result = try dictionary()

                completion(.success(result))
            } catch let error {
                completion(.failure(error))
            }
        }
    }

    /**
     Retrieve asynchronously an array at URL

     - Parameters:
     - completion: a Result<[Any]?, Error> -> Void
     */
    func array(completion: @escaping (Result<[Any]?, Error>) -> Void) {
        DispatchQueue.global(qos: .background).async {
            do {
                let result = try array()

                completion(.success(result))
            } catch let error {
                completion(.failure(error))
            }
        }
    }


}
