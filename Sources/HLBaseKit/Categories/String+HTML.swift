import Foundation

public extension String {
    /// Returns a String by stripping irrelevent characters such as <br>, <p>, etc.
    func strippingHTML() -> String {
        let replacedNextLine = replacingOccurrences(of: "<br>", with: "\n").replacingOccurrences(of: "<p>", with: "\n")
        let replacedNBSP = replacedNextLine.replacingOccurrences(of: "&nbsp;", with: " ")
        return replacedNBSP.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
}
