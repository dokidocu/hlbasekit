//
//  NSLocalizedString+Debug.swift
//  
//
//  Created by Henri LA on 09.02.20.
//

import Foundation

/// Returns a localized string, using the main bundle if one is not specified. When isDebug is true, then
/// the key is shown instead
public func NSLocalizedString(key: String,
                              comment: String,
                              bundle: Bundle = .main,
                              isDebug: Bool = false) -> String
{
  let tableName = isDebug ? "#DEBUG#" : nil
  return NSLocalizedString(key, tableName: tableName, bundle: bundle, value: "", comment: comment)
}
