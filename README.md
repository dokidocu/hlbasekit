# HLBaseKit

HLBaseKit is a kit / library / framework. It contains files that can be reused in iOS / tvOS / macOS frameworks or application. Files are dependent of Foundation kit from Apple. It supports iOS 12 +.

## Overview

HLBaseKit provides:

- Helpers functions of Foundation. (Array, Date, Int, Locale, OperationQueue, ...)
- Generic typealias (Closures)
- Local files reader to read file asynchronously or synchronously
- Protocols (DataSourceable, Loggable)
- OSLogger, a logger that uses **os_log** frameworks
- Network operator that supports certificat pinning with **TrustKit**
- Reachability

## Dependencies

- Foundation
- KeychainAccess : 4.2.1
- Trustkit : 1.7.0

## Installation

### Swift Package Manager (SPM)

Repository link for Xcode: https://bitbucket.org/dokidocu/hlbasekit

## Release notes