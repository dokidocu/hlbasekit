// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "HLBaseKit",
    platforms: [
      .iOS(.v12)
    ],
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "HLBaseKit",
            type: .dynamic,
            targets: ["HLBaseKit"])
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
      .package(url: "https://github.com/datatheorem/TrustKit", from: "1.7.0"),
      .package(url: "https://github.com/kishikawakatsumi/KeychainAccess.git", from: "4.2.1")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "HLBaseKit",
            dependencies: ["TrustKit", "KeychainAccess"]),
        .testTarget(
            name: "HLBaseKitTests",
            dependencies: ["HLBaseKit"]),
    ]
)
